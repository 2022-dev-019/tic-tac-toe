//
//  TicTacToeTests.swift
//  TicTacToeTests
//
//  Created by MacBook on 2/3/2022.
//

import XCTest
import SwiftUI
@testable import TicTacToe

class TicTacToeTests: XCTestCase {


    @ObservedObject<TicTacToeViewModel> var viewModel: TicTacToeViewModel = TicTacToeViewModel()

    func testPlayerXHorizontalWin() throws {

        viewModel.movesArray[0] = "X"
        viewModel.movesArray[3] = "0"
        viewModel.movesArray[1] = "X"
        viewModel.movesArray[5] = "0"
        viewModel.movesArray[2] = "X"
        let xWon = viewModel.isPlayerWonHorizontally(player: "X")
        XCTAssertEqual(xWon, true)
    }


    func testPlayerXVerticalWin() throws {
        viewModel.reset()
        viewModel.movesArray[0] = "X"
        viewModel.movesArray[2] = "0"
        viewModel.movesArray[3] = "X"
        viewModel.movesArray[5] = "0"
        viewModel.movesArray[6] = "X"
        let xPlayerWon = viewModel.isPlayerWonVertically(player: "X")
        XCTAssertEqual(xPlayerWon, true)
    }


    func testPlayerXDiagonalWin() throws {
        viewModel.reset()
        viewModel.movesArray[0] = "X"
        viewModel.movesArray[1] = "O"
        viewModel.movesArray[2] = "O"
        viewModel.movesArray[4] = "X"
        viewModel.movesArray[5] = "O"
        viewModel.movesArray[8] = "X"
        let xPlayerWon = viewModel.isPlayerWonDiagonally(player: "X")
        XCTAssertEqual(xPlayerWon, true)
    }


    func testGameInProgress() throws {
        viewModel.reset()
        viewModel.movesArray[0] = "X"
        viewModel.movesArray[1] = "O"
        viewModel.movesArray[4] = "X"
        viewModel.movesArray[8] = "0"
        viewModel.movesArray[5] = "X"
        viewModel.movesArray[3] = "0"
        viewModel.movesArray[6] = "X"
        viewModel.movesArray[2] = "0"
        let containsSpace = viewModel.movesArray.contains("")
        XCTAssertEqual(containsSpace, true)
    }


    func testGameOverTied() throws {
        viewModel.reset()
        viewModel.movesArray[0] = "X"
        viewModel.movesArray[1] = "O"
        viewModel.movesArray[4] = "X"
        viewModel.movesArray[8] = "0"
        viewModel.movesArray[5] = "X"
        viewModel.movesArray[3] = "0"
        viewModel.movesArray[6] = "X"
        viewModel.movesArray[2] = "0"
        viewModel.movesArray[7] = "X"
        let gameOverTiedMessage = viewModel.getGamePlayMessageAndToggleGameOver()
        XCTAssertEqual(gameOverTiedMessage, "Game over Tied")
        let containsSpace = viewModel.movesArray.contains("")
        XCTAssertEqual(containsSpace, false)
    }


    func testReset(){
        viewModel.reset()
        let xMoves = viewModel.movesArray.contains("X")
        let oMoves = viewModel.movesArray.contains("0")
        XCTAssertEqual(xMoves, false)
        XCTAssertEqual(oMoves, false)
    }


    func testPlayXWon(){
        viewModel.reset()
        viewModel.movesArray[0] = "X"
        viewModel.movesArray[2] = "0"
        viewModel.movesArray[3] = "X"
        viewModel.movesArray[5] = "0"
        viewModel.movesArray[6] = "X"
        let xWon = viewModel.isPlayerWon(player: "X")
        XCTAssertEqual(xWon, true)
    }
}
