## How to install, build and run TicTacToe application
1. Clone the project from gitlab
2. Navigate to the root directory of the project
3. Double click TicTacToe.xcodeproj file
4. Select your preferred device from the top menu
5. Click Start button from the top menu
6. Wait until the emulator is displayed and start playing
