//
//  TicTacToeApp.swift
//  TicTacToe
//
//  Created by MacBook on 2/3/2022.
//

import SwiftUI

@main
struct TicTacToeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
