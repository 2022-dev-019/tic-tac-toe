//
// Created by MacBook on 2/3/2022.
//

import Foundation
import SwiftUI


struct TicTacToeView: View {


    @ObservedObject var viewModel: TicTacToeViewModel = TicTacToeViewModel()
    @State var isPlaying = true

    var body: some View {
        VStack {
            LazyVGrid(columns: Array(repeating: GridItem(.flexible(), spacing: 15), count: 3), spacing: 15) {

                ForEach(0..<9, id: \.self) { index in
                    ZStack {
                        Color.green
                        Color.white.opacity(viewModel.movesArray[index] == "" ? 1 : 0)
                        Text(viewModel.movesArray[index])
                                .font(.system(size: 55))
                                .fontWeight(.heavy)
                                .foregroundColor(.white)
                                .opacity(viewModel.movesArray[index] != "" ? 1 : 0)
                    }
                            .frame(width: getWidth(), height: getWidth())
                            .cornerRadius(100)
                            .onTapGesture(perform: {
                                if viewModel.movesArray[index] == "" {
                                    viewModel.movesArray[index] = isPlaying ? "X" : "0"
                                    isPlaying.toggle()
                                }
                            })
                }
            }.padding(15)
        }.onChange(of: viewModel.movesArray, perform: { value in
                    viewModel.getGamePlayMessageAndToggleGameOver()
                })
                .alert(isPresented: $viewModel.isGameOver, content: {
                    Alert(title: Text("TicTacToe"), message: Text(viewModel.msg), dismissButton:
                    .destructive(Text("Play Again"), action: {
                        withAnimation(Animation.easeIn(duration: 0.5)) {
                            viewModel.reset()
                            isPlaying = true
                        }
                    }))
                })
    }


    func getWidth() -> CGFloat {
        let width = UIScreen.main.bounds.width - (30 + 30)
        return width / 3
    }
}
