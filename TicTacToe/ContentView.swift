//
//  ContentView.swift
//  TicTacToe
//
//  Created by MacBook on 2/3/2022.
//

import SwiftUI

struct ContentView: View {

    var body: some View {
        TicTacToeView()
                .navigationTitle("Tic Tac Toe")
                .preferredColorScheme(.dark)
    }
}

