//
// Created by MacBook on 2/3/2022.
//

import Foundation


class TicTacToeViewModel: ObservableObject{


    @Published public var movesArray: [String] = Array(repeating: "", count: 9);
    @Published public var isGameOver: Bool = false;
    @Published public var msg = "";


    func isPlayerWonHorizontally(player: String) -> Bool {
        for i in stride(from: 0, to: 9, by: 3) {
            if movesArray[i] == player && movesArray[i + 1] == player && movesArray[i + 2] == player {
                return true
            }
        }
        return false;
    }

    func isPlayerWonVertically(player: String) -> Bool {
        for i in 0...2 {
            if movesArray[i] == player && movesArray[i + 3] == player && movesArray[i + 6] == player {
                return true
            }
        }
        return false;
    }

    func reset() -> Void{
        movesArray.removeAll()
        movesArray = Array(repeating: "", count: 9)
    }

    func isPlayerWonDiagonally(player: String) -> Bool {
        /*
         X 0 0
         0 X 0
         0 0 X
         */
        if movesArray[0] == player && movesArray[4] == player && movesArray[8] == player {
            return true
        }
        /*
        0 0 X
        0 X 0
        X 0 0
        */
        if movesArray[2] == player && movesArray[4] == player && movesArray[6] == player {
            return true
        }
        return false;
    }


    func isPlayerWon(player: String) -> Bool {
        if (isPlayerWonHorizontally(player: player)) {
            return true;
        } else if (isPlayerWonVertically(player: player)) {
            return true;
        } else if (isPlayerWonDiagonally(player: player)) {
            return true;
        }
        return false
    }


    func getGamePlayMessageAndToggleGameOver() -> String {
        /*
         We have two players
            First Player  X
            Second Player 0
        */
        if isPlayerWon(player: "X") {
            msg = "Player X Won"
            isGameOver.toggle()
        } else if isPlayerWon(player: "0") {
            msg = "Player 0 Won"
            isGameOver.toggle()
        } else {
            // check for no moves
            let status = movesArray.contains { (value) -> Bool in
                return value == ""
            }
            if !status {
                msg = "Game over Tied"
                isGameOver.toggle()
            }
        }
        return msg;
    }

}
